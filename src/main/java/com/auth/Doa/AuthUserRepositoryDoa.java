package com.auth.Doa;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.auth.Entity.UserAuth;

@Component
public interface AuthUserRepositoryDoa extends CrudRepository<UserAuth,String>{
	
	@Query(value="SELECT CASE WHEN count(*)=1 THEN true ELSE false END FROM user_auth where user_device_details=:e",nativeQuery=true)
    int existsByUserDeviceDetails(@Param("e") String UserDeviceDetails);
	//checks whether the UserDeviceDetails(mobile number+device name+device id) exists in the database

	@Query(value="select CASE WHEN expiry_time > current_timestamp() THEN true ELSE false END FROM user_auth where user_device_details=:e",nativeQuery=true)
	int verifyAuthTokenExpiry(@Param("e") String UserDeviceDetails);
	//checks if current time of login is within the expiry time(stored in database)

	@Query(value="select CASE WHEN expiry_time > current_timestamp() THEN true ELSE false END FROM user_auth where auth_token=:e and device_id=:f",nativeQuery=true)
	int validateAuthTokenExpiry(@Param("e") String AuthToken, @Param("f") String deviceId);
	//validates auth token for LOS and PS if user is accessing those services within the expiry time

	@Query(value="select auth_token from user_auth where user_device_details=:udd",nativeQuery=true)
	String getValidAuthToken(@Param("udd") String UserDeviceDetails);
	//returns auth-token for user for given user-device-details
	
	@Query(value="select CASE WHEN current_timestamp() < :c THEN true ELSE false END",nativeQuery=true)
	int CheckOtpExpiry(@Param("c") Timestamp time);
	//checks if current time of entering OTP is within the OTP expiry time
	
	@Query(value="select user_id from user_auth where user_device_details=:udd",nativeQuery=true)
	int getUserId(@Param("udd") String userDeviceDetails);
	//returns user-id for user for given user-device-details

}
