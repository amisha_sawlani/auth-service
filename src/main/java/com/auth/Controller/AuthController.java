package com.auth.Controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Optional;

import com.auth.Bodies.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.auth.Service.AuthService;


@RestController
public class AuthController {
	private String userMobileNumber;
	private String userDeviceID;
	private String userDeviceModel;
	private long user_Id;

	@Autowired 
	AuthService authService;

	@Autowired
	loginResponse login_Response;
	
	@Autowired
	OtpController otp;
	
	@Autowired
	ComRequestBody requestBody;

	@Autowired
	RequestEntityCheckUser identityRequest;



	RestTemplate restTemplate=new RestTemplate();

	// Login Endpoint for user
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody UserLoginDetailInput userInput) {
		// checks for the valid phone number
		if(!this.authService.ChecknumberValidity(userInput.getUser_name(),userInput.getDevice_model(),userInput.getDevice_id())){
			return ResponseEntity.of(Optional.of("Invalid Username. Please enter valid phone number !!"));
		}

		userMobileNumber=userInput.getUser_name();
		userDeviceModel = userInput.getDevice_model();
		userDeviceID = userInput.getDevice_id();
		boolean otpSent = false;

		//case when user exists
		if(this.authService.checkUserExists(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID)==1) {

			//case when user is logged in and still has the valid auth token
			if(this.authService.verifyTokenExpiry(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID)==1) {
				// return here AuthToken and device id
				login_Response.setAuthToken(this.authService.getValidAuthToken(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID));
				login_Response.setDeviceId(userDeviceID);
				return ResponseEntity.of(Optional.of(login_Response));
			}

			//case when user auth-token is expired
			//generate OTP, set the request type,mobile number, otp and user_id for communication service
			user_Id=this.authService.getByUserId(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID);
			int generatedOtp=this.authService.generateOTP();
			this.authService.storeGeneratedOTP(generatedOtp);
			requestBody.setRequestType("LOTP");
			HashMap<String,String> temp=new HashMap<String,String>();
			temp.put("mobileNumber",userMobileNumber);
			temp.put("otp",String.valueOf(generatedOtp));
			temp.put("userId",Long.toString(user_Id));
			requestBody.setDetails(temp);

			//send required credentials(OTP, request type,mobile number,user_id ) to communication service
			try{
				final String otpurl="http://10.70.4.178:8080/sendSMS";
				RestTemplate restTemplate=new RestTemplate();
				ComResponseBody isResponse=restTemplate.postForObject(otpurl,requestBody,ComResponseBody.class);
				otpSent = isResponse.isStatus();

			}catch(Exception e){
				HashMap<String,Integer> hm = new HashMap<>();
				hm.put("Status",HttpStatus.INTERNAL_SERVER_ERROR.value());
				return ResponseEntity.of(Optional.of(hm));
			}
			System.out.println(otpSent);

			//case when communication service sends the otp and returns the status
			if(otpSent) {

				//set the otp expiry time
				Timestamp otpexpiry = this.authService.getOtpExpiryTime();
				System.out.println(generatedOtp+" is sent to "+userMobileNumber+"and otp expires at "+this.authService.getOtpExpiryTime());
				
				System.out.println(this.authService.checkOtpExpiry(otpexpiry));

				//case when the otp is not expired
				while(this.authService.checkOtpExpiry(otpexpiry)==1) {

					//case when the user entered otp
					if(this.otp.getUserOtp()!=0) {

						//case when generated otp and user entered otp are same: set user otp, auth-token, device-id
						if(this.authService.verifyOtp(generatedOtp, this.otp.getUserOtp())) {
							this.otp.setUserOtp(0);
							login_Response.setAuthToken(this.authService.saveUser(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID,user_Id,this.authService.generateAuthToken(),userDeviceID));
							login_Response.setDeviceId(userDeviceID);
							return ResponseEntity.of(Optional.of(login_Response));
						}
						System.out.println("OTP in correct");
						//return ResponseEntity.of(Optional.of("Incorrect OTP. So, Please login again"));
					}
				}

				//case when otp is expired
				System.out.println("OTP Expired. So, please login again :)");
				return ResponseEntity.of(Optional.of("OTP Expired. So, please login again :)"));
			}

			//case when communication service doesn't acknowledge the status of OTP being sent
			return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).build();
		}

		//case when user doesn't exist in the auth database
		// call identity service to check if New user or New Device
		identityRequest.setMobile(userMobileNumber);
		identityRequest.setDeviceModel(userDeviceModel);
		identityRequest.setDeviceId(String.valueOf(userDeviceID));
		System.out.println("Hello this is IS");

		//call identity service and retrieve the user_id they respond with
		try {
			String isUrl="http://10.70.4.165:8080/api/user/checkUserExists";
			ApiResponseCheckUser identityResponse= restTemplate.postForObject(isUrl, identityRequest, ApiResponseCheckUser.class);
			System.out.println(identityResponse.getMessage());
			user_Id=identityResponse.getUserId();
			System.out.println(user_Id);
		}catch(Exception e) {
			//e.printStackTrace();
			HashMap<String,Integer> hm = new HashMap<>();
			hm.put("Status",HttpStatus.INTERNAL_SERVER_ERROR.value());
			return ResponseEntity.of(Optional.of(hm));
		}

		//case when user_id<0 meaning user doesn't exist, allow user to signUp
		if(user_Id<0) {
			return ResponseEntity.of(Optional.of("Go to Signup"));
		}

		//case when mobile number exists with identity service and they acknowledge with user_id
		//generate OTP, set the request type,mobile number, otp and user_id
		int generatedOtp = this.authService.generateOTP();
		this.authService.storeGeneratedOTP(generatedOtp);
		requestBody.setRequestType("LOTP");
		HashMap<String,String> temp=new HashMap<String,String>();
		temp.put("mobileNumber",userMobileNumber);
		temp.put("otp",String.valueOf(generatedOtp));
		temp.put("userId",Long.toString(user_Id));
		requestBody.setDetails(temp);		//call comm API

		//send required credentials(OTP, request type,mobile number,user_id ) to communication service
		try{
			final String otpurl="http://10.70.4.178:8080/sendSMS";
			RestTemplate restTemplate=new RestTemplate();
			ComResponseBody isResponse=restTemplate.postForObject(otpurl,requestBody,ComResponseBody.class);
			otpSent=isResponse.isStatus();
		}catch (Exception e){
			HashMap<String,Integer> hm = new HashMap<>();
			hm.put("Status",HttpStatus.INTERNAL_SERVER_ERROR.value());
			return ResponseEntity.of(Optional.of(hm));
		}

		//case when communication service sends the otp and returns the status
		if(otpSent) {

			//set the otp expiry time
			System.out.println("otp sent by communication Api");
		    Timestamp otpexpiry = this.authService.getOtpExpiryTime();
			System.out.println(generatedOtp+" is sent to "+userMobileNumber+" and otp expires at "+this.authService.getOtpExpiryTime());
			
			System.out.println(this.authService.checkOtpExpiry(otpexpiry));

			//case when the otp is not expired
			while(this.authService.checkOtpExpiry(otpexpiry)==1) {

				//case when the user entered otp
				if(this.otp.getUserOtp()!=0) {

					//case when generated otp and user entered otp are same: generate auth-token
					if(this.authService.verifyOtp(generatedOtp, this.otp.getUserOtp())) {
						this.otp.setUserOtp(0);
						login_Response.setAuthToken(this.authService.saveUser(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID,user_Id,this.authService.generateAuthToken(),userDeviceID));
						login_Response.setDeviceId(userDeviceID);
						return ResponseEntity.of(Optional.of(login_Response));
					}
					//return ResponseEntity.of(Optional.of("Incorrect OTP. So, Please login again"));
				}
			}

			//case when otp is expired
			System.out.println("OTP Expired. So, please login again :)");
			return ResponseEntity.of(Optional.of("OTP Expired. So, please login again :)"));
		}

		//case when communication service doesn't acknowledge the status of OTP being sent
		return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).build();
		
	}

	// Validate Token Endpoint.
//	@RequestMapping(value="/validateToken", method=RequestMethod.POST) //API will be called by LOS and PS
//	public ComResponseBody validateToken(@RequestBody ValidateTokenRequest validateTokenRequest) {
//		System.out.println("hello, u called me!!");
//
//			if(authService.validateToken(validateTokenRequest.getAuthToken(),validateTokenRequest.getDeviceId())==1) // if token is
//				return new ComResponseBody(true);
//		return new ComResponseBody(false);
//
//	}

	//API will be called by LOS and PS to validate authtoken
	@RequestMapping(value="/validateToken", method=RequestMethod.POST)
	public HashMap<String,Integer> validateToken(@RequestBody HashMap<String,String> validateTokenRequest) {
		System.out.println("hello, u called me!!");
		HashMap<String,Integer> h = new HashMap<>();

		//if the token is validated
		try{
			if(authService.validateToken(validateTokenRequest.get("auth_token"),validateTokenRequest.get("device_id"))==1) // if token is
			{
				h.put("Status",HttpStatus.OK.value());
				return h;
			}
		}

		//if the token is validated
		catch (Exception e){
			System.out.println("its invalid token !!");

		}
		h.put("Status",HttpStatus.UNAUTHORIZED.value());
		return h;


	}
}
