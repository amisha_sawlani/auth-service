package com.auth.Controller;

import com.auth.Bodies.OtpRequest;
import com.auth.Service.AuthService;
import com.auth.Service.AuthServiceImple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Optional;

@Component
@RestController
public class OtpController {
	private int userOtp;

	@Autowired
	AuthService authService;

	public int getUserOtp() {
		return userOtp;
	}

	public void setUserOtp(int userOtp) {
		this.userOtp = userOtp;
	}

	//accepts OTP from user and checks if user entered otp and generated otp are equal
	@PostMapping("/EnterOtp")
	public ResponseEntity<?> EnterOtp(@RequestBody OtpRequest otp) { //HttpMessageNotReadableException
		this.setUserOtp(otp.getOtp());
		HashMap<String,String> hm=new HashMap<>();
		if(this.getUserOtp()==authService.getStoredGeneratedOTP()){
			hm.put("AuthResponse","Login successful !!");
			return ResponseEntity.of(Optional.of(hm));
		}
		hm.put("AuthResponse",this.getUserOtp()+" Entered by user was incorrect. Please enter again");
		return ResponseEntity.of(Optional.of(hm));
	}

	public OtpController(int userOtp) {
		super();
		this.userOtp = userOtp;
	}

	public OtpController() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
